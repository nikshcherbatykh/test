<?php

class Luxinten_Testtask_Adminhtml_DisplayController extends Mage_Adminhtml_Controller_Action
{

    protected function _initAction()
    {
        $this->loadLayout()->_setActiveMenu('testtask/items');
        return $this;
    }

    public function indexAction()
    {
        $this->_initAction();

        $contentBlock = $this->getLayout()->createBlock('testtask/adminhtml_testtask');
        $this->_addContent($contentBlock);
        $this->renderLayout();
    }

}