<?php

class Luxinten_Testtask_IndexController extends Mage_Core_Controller_Front_Action
{

    public function saveAction()
    {
        $info = $this->getRequest()->getPost('info');
        $productName = $this->getRequest()->getPost('product_name');
        $data = array('info' => $info, 'product_name' => $productName);
        $response = new Varien_Object();

        if (!empty($data['info']) && !empty($data['product_name'])) {
            Mage::getModel('testtask/info')->setData($data)->save();
            $response->addData(array('success' => true, 'message' => 'Luxinten Test has been saved.'));
        } else {
            $response->addData(array('success' => false, 'message' => 'Luxinten Test field is empty.'));
        }

        $this->getResponse()->setBody($response->toJson());

    }

    public function testModelAction()
    {
        echo 'id product_name info';
    }
}