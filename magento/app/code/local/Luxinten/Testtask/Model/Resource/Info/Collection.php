<?php

class Luxinten_Testtask_Model_Resource_Info_Collection extends Mage_Core_Model_Resource_Db_Collection_Abstract
{
    protected function _construct()
    {
        $this->_init('testtask/info');
    }
}