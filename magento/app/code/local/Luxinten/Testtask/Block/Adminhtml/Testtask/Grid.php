<?php

class Luxinten_Testtask_Block_Adminhtml_Testtask_Grid extends Mage_Adminhtml_Block_Widget_Grid
{
    protected function _prepareCollection()
    {
        $collection = Mage::getModel('testtask/info')->getCollection();
        $this->setCollection($collection);
        return parent::_prepareCollection();
    }

    protected function _prepareColumns()
    {

        $helper = Mage::helper('testtask');

        $this->addColumn('id', array(
            'header' => $helper->__('Info ID'),
            'index' => 'id'
        ));

        $this->addColumn('product name', array(
            'header' => $helper->__('Product Name'),
            'index' => 'product_name',
            'type' => 'text'
        ));

        $this->addColumn('info', array(
            'header' => $helper->__('Info'),
            'index' => 'info',
            'type' => 'text'
        ));

        return parent::_prepareColumns();
    }

}