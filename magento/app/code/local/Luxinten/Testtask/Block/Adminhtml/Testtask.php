<?php

class Luxinten_Testtask_Block_Adminhtml_Testtask extends Mage_Adminhtml_Block_Widget_Grid_Container
{

    protected function _construct()
    {
        parent::_construct();

        $helper = Mage::helper('testtask');
        $this->_blockGroup = 'testtask';
        $this->_controller = 'adminhtml_testtask';
        $this->_headerText = $helper->__('Info Management');
        $this->_addButtonLabel = $helper->__('Add Info');
    }

}